
for user in `cut -d: -f1 /etc/trueuserowners`; do

    for domain in `grep -w $user /etc/userdomains | cut -d: -f1`; do

        DOCROOT=`whmapi1 domainuserdata domain=$domain --output=json | jq -r '.data | .userdata | .documentroot'`;

        if su -s /bin/bash $user -c "wp user list --path=$DOCROOT" | grep -wq dexter
        then 
            echo $domain >> dexter_domains.list; 
        fi

    done;

done;
