#!/bin/bash


urlencode() {
    # urlencode <string>

    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%s' "$c" | xxd -p -c1 |
                   while read c; do printf '%%%s' "$c"; done ;;
        esac
    done
}

urldecode() {
    # urldecode <string>

    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}

if [ "$#" -eq 0 ] ; then
        echo "Please specify the filename to proceed"
elif [ "$#" -eq 1 ] ; then
        arg=$(echo $1)
        echo "SUBMITTING TASK ::::   action=restore&ip=%38%31%2E%31%39%2E%32%31%35%2E%31%37&ip%5Fchoice=select&local%5Fpath=$(echo "$(urlencode $arg)")&type=admin&value=multiple&when=now&where=local"
        echo "action=restore&ip=%38%31%2E%31%39%2E%32%31%35%2E%31%37&ip%5Fchoice=select&local%5Fpath=$(echo "$(urlencode $arg)")&type=admin&value=multiple&when=now&where=local" >> /usr/local/directadmin/data/task.queue
        /usr/local/directadmin/dataskq d2000
fi
