# /bin/bash

# bash <(wget -qO- https://gitlab.com/brixly/file-dump/-/raw/master/scripts/set_lve_alerts.sh)

wget -O /usr/share/lve/emails/en_US/admin_notify.html http://repo.brixly.uk/alert_templates/admin_notify.html
wget -O /usr/share/lve/emails/en_US/admin_notify.txt http://repo.brixly.uk/alert_templates/admin_notify.txt
wget -O /usr/share/lve/emails/en_US/reseller_faults_notify.html http://repo.brixly.uk/alert_templates/reseller_faults_notify.html
wget -O /usr/share/lve/emails/en_US/reseller_faults_notify.txt http://repo.brixly.uk/alert_templates/reseller_faults_notify.txt
wget -O /usr/share/lve/emails/en_US/reseller_notify.html http://repo.brixly.uk/alert_templates/reseller_notify.html
wget -O /usr/share/lve/emails/en_US/reseller_notify.txt http://repo.brixly.uk/alert_templates/reseller_notify.txt
wget -O /usr/share/lve/emails/en_US/user_notify.html http://repo.brixly.uk/alert_templates/user_notify.html
wget -O /usr/share/lve/emails/en_US/user_notify.txt http://repo.brixly.uk/alert_templates/user_notify.txt
