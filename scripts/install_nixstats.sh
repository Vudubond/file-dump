# /bin/bash

systemctl stop nixstatsagent
systemctl disable nixstatsagent
rm -f /etc/systemd/system/nixstatsagent
rm -f /etc/nixstats.ini
rm -f /etc/nixstats-token.ini
pip uninstall nixstatsagent
userdel nixstats
groupdel nixstats

apt install python3-pip
pip3 install nixstatsagent --upgrade

wget -q -N --no-check-certificate https://nixstats.com/nixstatsagent.sh && bash nixstatsagent.sh 5c9a2cdf3f0bc1367b293c79 DVRTRj9DCDDDb7ud1UMamAiz

# nixstats - Run Agent as Root

sed -i "s\User=nixstats\User=root\g" /etc/systemd/system/nixstatsagent.service; systemctl daemon-reload; service nixstatsagent restart;

# nixstats - CL support for Process Logs
#echo fs.proc_super_gid=$(id nixstats | awk -F"gid=" '{print $2}' | awk -F"(" '{print $1}') >> /etc/sysctl.conf
#sysctl -p
#echo nixstats >> /etc/cagefs/exclude/systemuserlist
#cagefsctl --force-update
#service lve_namespaces restart
#sudo -u nixstats nixstatsagent test process
#service nixstatsagent restart

# nixstats - Exim Monitoring

echo "nixstats ALL=(ALL) NOPASSWD: /usr/sbin/exim" >> /etc/sudoers

# nixstats - CL User Monitoring

echo "nixstats ALL=(ALL) NOPASSWD: /usr/sbin/lveinfo" >> /etc/sudoers

# nixstats - Reset nixstats.ini
wget -O /etc/nixstats.ini https://gitlab.com/brixly/file-dump/raw/master/nixstats.ini
service nixstatsagent restart
