for disk in $(lsblk | grep disk | awk '{print $1}'); do
    print "Running grub2-install on $disk"
    grub2-install /dev/$disk
done;

grub2-mkconfig -o /boot/grub2/grub.cfg
grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
