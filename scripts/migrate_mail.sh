# !/bin/bash

if [ "$#" -eq 0 ] ; then
        echo "Please specify the following three parameters...\n"
        echo "Parameter 1: Remote Server (hostname or IP)\n"
        echo "Parameter 2: Remote Username\n"
        echo "Parameter 3: Local Username\n"
        echo "Parameter 4: Domain Name\n"
elif [ "$#" -eq 4 ] ; then
        remoteserver=$(echo $1)
        remoteuser=$(echo $2)
        localuser=$(echo $3)
        domain=$(echo $4)
        # Rsync from the remote server...
        sshpass -Ppassphrase -f <(printf '%s\n' $(cat /root/.ssh/id_internal.phr)) rsync --progress -avz -e "ssh -i /root/.ssh/id_internal" root@$remoteserver:/home/$remoteuser/mail/$domain /home/$localuser/mail/$domain/

else
        echo "Invalid number of parameters"
fi
