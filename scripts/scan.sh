if [ "$#" -eq 0 ] ; then

        printf "\n"
        read -p "Do you want to run a scan of the entire server? " -n 1 -r
        echo    # (optional) move to a new line
        if [[ $REPLY =~ ^[Yy]$ ]]
        then
                bitninjacli --module=MalwareScanner --scan=/home/
        fi

elif [ "$#" -eq 1 ] ; then
        arg=$(echo $1)
        echo $1|grep '@'
        if [ $? -eq 0 ] ; then
                whmapi1 create_user_session user=$arg service=webmaild
        else
                        bitninjacli --module=MalwareScanner --scan=/home/$arg/public_html
                #fi
        fi
else
        echo "Invalid number of parameters"
fi
