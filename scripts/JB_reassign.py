#!/usr/bin/python3

import subprocess
import json

accounts=json.loads(subprocess.getoutput("jetbackup5api -F listAccounts -O json"))['data']['accounts']

for account in accounts:
    assignable=json.loads(subprocess.getoutput(f"jetbackup5api -F listAssignableAccounts -D account={account['username']} -O json"))['data']['accounts']
    for id in assignable:
        if id['total_backups'] > 10:
            print(json.loads(subprocess.getoutput(f"jetbackup5api -F reassignAccount -D _id={id['_id']} -O json")))

