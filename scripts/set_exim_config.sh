#!/bin/bash

# bash <(curl -s https://gitlab.com/brixly/file-dump/raw/master/scripts/set_exim_config.sh)

wget -O /etc/exim.conf.local https://gitlab.com/brixly/file-dump/-/raw/master/exim/exim.conf.local
wget -O /etc/exim.conf.localopts https://gitlab.com/brixly/file-dump/-/raw/master/exim/exim.conf.localopts
wget -O /usr/local/cpanel/etc/exim/acls/ACL_RECP_VERIFY_BLOCK/custom_begin_recp_verify https://gitlab.com/brixly/file-dump/-/raw/master/exim/custom_begin_recp_verify
wget -O /opt/setest https://gitlab.com/brixly/file-dump/-/raw/master/exim/setest
wget -O /opt/setestptr https://gitlab.com/brixly/file-dump/-/raw/master/exim/setestptr

# Perform check to see if we are running Exim 4.94
version=$(exim --version)
if [[ $version == *"4.94"* ]]; then
  sed '/message_linelength_limit/d' /etc/exim.conf.local -i
fi

chmod +x /opt/setest
chmod +x /opt/setestptr

/scripts/buildeximconf
systemctl restart exim
