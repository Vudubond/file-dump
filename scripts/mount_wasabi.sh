#/bin/bash

yum install s3fs-fuse -y
mkdir -p /mnt/wasabi
s3fs $(hostname) /mnt/wasabi/ -o passwd_file=/etc/passwd-s3fs -o url=https://s3.eu-central-1.wasabisys.com -o use_path_request_style
