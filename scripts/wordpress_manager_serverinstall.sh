# /bin/bash
# bash <(wget -qO- https://gitlab.com/brixly/file-dump/-/raw/master/scripts/wordpress_manager_serverinstall.sh)

wget -O /root/WordPressManager_server.zip https://gitlab.com/brixly/file-dump/-/raw/master/WordPressManager_server.zip?inline=false
unzip /root/WordPressManager_server

service_exists() {
    local n=$1
    if [[ $(systemctl list-units --all -t service --full --no-legend "$n.service" | cut -f1 -d' ') == $n.service ]]; then
        return 0
    else
        return 1
    fi
}
if service_exists directadmin; then
    echo "DirectAdmin Detected - Installing Requirements..."
    tar -xvzf /root/uploadToDirectAdmin/wordpress_cli.tar.gz -C /root/uploadToDirectAdmin/
    bash /root/uploadToDirectAdmin/scripts/install.sh
fi

if service_exists cpanel; then
    echo "cPanel Detected - Installing Requirements..."
    tar -xvzf /root/uploadToDirectAdmin/wordpress_cli.tar.gz -C /root/uploadToWHM/
    bash /root/uploadToWHM/install.sh
fi
