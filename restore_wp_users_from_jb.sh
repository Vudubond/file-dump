#/bin/bash
domain=$1
user=$(/scripts/whoowns $domain)
db_name=""
table_prefix=""
backup_dir=""
backup_path=""
latest_backup_dir=""
backup_file=""
working_dir=""
DOCROOT=`whmapi1 domainuserdata domain=$domain --output=json | jq -r '.data | .userdata | .documentroot'`;
db_name=$(su -s /bin/bash $user -c "wp config list --path=$DOCROOT" | grep DB_NAME | awk '{print $2}')
table_prefix=$(su -s /bin/bash $user -c "wp config list --path=$DOCROOT" | grep table_prefix | awk '{print $2}')
backup_dir=$(ls -1dt /mnt/backup-box/jetbackup_1_1_619cbbb07d00444cd259c712/* | grep "$user"_ | head -n 1)
backup_path=$backup_dir
latest_backup_dir=$(\ls -1dt $backup_path/snap* | sed -n '2p')

backup_file=$latest_backup_dir/database/$db_name.sql.gz

# Show the 'before' user list
printf "$user - Before Change...\n"

# Find out if dexter is found in the list of wp users

su -s /bin/bash $user -c "wp user list --path=$DOCROOT"

read -p "Do you wish to continue? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then

    # Move a copy of the database backup locally, and gunzip

    working_dir=/root/DEXTER_INVESTIGATION/database_recovery/$user/
    mkdir -p $working_dir
    cp $backup_file $working_dir/.
    gunzip $working_dir/$db_name.sql.gz

    # Extract users table from the database
    cd $working_dir
    bash /root/DEXTER_INVESTIGATION/mysql_split.sh $working_dir/$db_name.sql $table_prefix"users"
    mysql $db_name < $working_dir/$table_prefix"users".sql

    printf "$user - After Change...\n"

    su -s /bin/bash $user -c "wp user list --path=$DOCROOT"

fi
