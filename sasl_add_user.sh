# !/bin/bash

# Create a backup of a user account, and move this to their home directory
if [ "$#" -eq 0 ] ; then
        echo "Please specify the username you wish to add to SASL for LSMCD"
elif [ "$#" -eq 1 ] ; then
        arg=$(echo $1)
        passwd=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 10 ; echo '')
        echo $passwd | saslpasswd2 -p -f /etc/sasllsmcd $arg
elif [ "$#" -eq 2 ] ; then
        arg=$(echo $1)
else
        echo "Invalid number of parameters"
fi
